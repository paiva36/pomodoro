CREATE TABLE IF NOT EXISTS tarefa ( _id integer primary key autoincrement, nome text not null, descricao text not null, qtd_pomodoro integer not null, status text);
INSERT INTO tarefa (nome,descricao, qtd_pomodoro, status) VALUES ('Estudar para Certificação', 'Iniciar estudo de threads para certificação java',5,'INICIADO');
INSERT INTO tarefa (nome,descricao, qtd_pomodoro, status) VALUES ('Estudar para Concurso', 'Iniciar estudo de redes para concurso',10,'INICIADO');
INSERT INTO tarefa (nome,descricao, qtd_pomodoro, status) VALUES ('Fazer Atividade Fisica', 'Iniciar atividade fisica de musculação',8,'INICIADO');
