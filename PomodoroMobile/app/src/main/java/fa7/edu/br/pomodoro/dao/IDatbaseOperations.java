package fa7.edu.br.pomodoro.dao;

import java.util.List;

/**
 * Created by bruno on 30/08/15.
 */
public interface IDatbaseOperations<T> {

    void insert(T obj);

    void update(T obj);

    void delete(T obj);

    T find(Long id);

    List<T> findAll();

}
