package fa7.edu.br.pomodoro.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import fa7.edu.br.pomodoro.R;
import fa7.edu.br.pomodoro.fragment.FragmentCronometro;
import fa7.edu.br.pomodoro.fragment.FragmentTarefaForm;
import fa7.edu.br.pomodoro.fragment.FragmentTarefaMain;


public class MainActivity extends AppCompatActivity {

    private FragmentManager mFragmentManager;
    private final FragmentTarefaMain mFragmentTarefaMain;
    private final FragmentTarefaForm mFragmentTarefaForm;
    private final FragmentCronometro mFragmentCronometro;

    public MainActivity() {
        this.mFragmentTarefaMain = new FragmentTarefaMain();
        this.mFragmentTarefaForm = new FragmentTarefaForm();
        this.mFragmentCronometro = new FragmentCronometro();
    }

    public FragmentCronometro getmFragmentCronometro() {return mFragmentCronometro;}

    public FragmentTarefaForm getmFragmentTarefaForm() {
        return mFragmentTarefaForm;
    }

    public FragmentTarefaMain getmFragmentTarefaMain() {
        return mFragmentTarefaMain;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragmentManager = getSupportFragmentManager();
        // Criando e exbibindo fragmento principal...
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_content, mFragmentTarefaMain);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        switch (id) {
            case R.id.action_settings:
                break;
            case android.R.id.home:
                fragmentTransaction.replace(R.id.main_content,mFragmentTarefaMain);
                break;
        }
        fragmentTransaction.commit();
        return true;
    }

}
