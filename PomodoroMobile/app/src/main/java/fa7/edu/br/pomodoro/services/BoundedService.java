package fa7.edu.br.pomodoro.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

//import android.support.annotation.Nullable;

/**
 * Created by bruno on 26/08/15.
 */
public class BoundedService extends Service {

    private IBinder binder;
    private boolean stop;

    public BoundedService() {
        this.binder = new LocalBinder();
        this.stop = false;
    }

    public void start() {

        this.stop = false;

        new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (!stop) {
                    try {
                        Log.i("APP", "Valor: " + i++);
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void stop() {
        this.stop = true;
    }

    //@Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class LocalBinder extends Binder {
        public BoundedService getService() {
            return BoundedService.this;
        }
    }

}
