package fa7.edu.br.pomodoro.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import fa7.edu.br.pomodoro.R;
import fa7.edu.br.pomodoro.services.BoundedService;

/**
 * Created by charlespaiva on 15/09/15.
 */
public class FragmentConfiguracoes extends Fragment implements OnClickListener {

    private AppCompatActivity mMainActivity;
    private Toolbar mMainToolbar;
    private Button mStartedService;
    private Button mStartedBoundService;
    private BoundedService mBoundedService;
    private boolean mIsServiceBounded;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_configuracoes, container, false);

        mMainActivity = (AppCompatActivity) getActivity();

        mMainToolbar = (Toolbar) mMainActivity.findViewById(R.id.main_toolbar);
        mMainToolbar.setTitle("Configuracoes");
        mMainToolbar.setLogo(null);
        mMainActivity.setSupportActionBar(mMainToolbar);
        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return v;
    }

    @Override
    public void onClick(View v) {

    }
}
