package fa7.edu.br.pomodoro.model;

import java.io.Serializable;

/**
 * Created by paiva on 09/09/2015.
 */
public class Tarefa implements Serializable {


    public enum Status {
        NENHUM, INICIADO, CONCLUIDO
    }

    private Long id;

    private String nome;

    private String descricao;

    private Long qtdPomodoros;

    private Status status;

    public Tarefa() {
    }

    public Tarefa(String nome, String descricao, Long qtdPomodoros) {
        this.status = Status.NENHUM;
        this.nome = nome;
        this.descricao = descricao;
        this.qtdPomodoros = qtdPomodoros;
    }

    public Tarefa(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Tarefa(Long id, String nome, String descricao, Long qtdPomodoros, Status status) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.qtdPomodoros = qtdPomodoros;
        this.status = status;
    }

    public Tarefa(Long id, String nome, String descricao, Long qtdPomodoros) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.qtdPomodoros = qtdPomodoros;
        this.status = Status.NENHUM;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getQtdPomodoros() {
        return qtdPomodoros;
    }

    public void setQtdPomodoros(Long qtdPomodoros) {
        this.qtdPomodoros = qtdPomodoros;
    }
}
