package fa7.edu.br.pomodoro.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fa7.edu.br.pomodoro.model.Tarefa;


/**
 * Created by paiva on 09/09/2015.
 */
public class TarefaDao extends GenericDAO<Tarefa> {

    private static final String TABLE_NAME = "tarefa";

    public TarefaDao(Context context) {
        super(context);
    }

    public void insert(Tarefa obj) {
        db.insert(TABLE_NAME, null, createContentValue(obj));
    }

    @Override
    public void delete(Tarefa tarefa) {
        int i = db.delete(TABLE_NAME, "_id like ? ", new String[]{String.valueOf(tarefa.getId())});
        Log.i("[TarefaDao][delete]", i + " objetos deletados !!");
    }

    @Override
    public void update(Tarefa tarefa) {
        String selection = "_id = ? ";
        String[] selectionArgs = {tarefa.getId().toString()};
        int i = db.update(TABLE_NAME, createContentValue(tarefa), selection, selectionArgs);
        Log.i("[TarefaDao][update]", i + " objetos atualizados !!");
    }

    @Override
    public Tarefa find(Long id) {
        Tarefa tarefa = null;
        Cursor cursor = db.query(TABLE_NAME,null,"_id = ?",new String[]{String.valueOf(id)},null,null,null);
        if (cursor.getCount() > 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                Long Id = cursor.getLong(cursor.getColumnIndex("_id"));
                String nome = cursor.getString(cursor.getColumnIndex("nome"));
                String descricao = cursor.getString(cursor.getColumnIndex("descricao"));
                Long qtdPomodoro = cursor.getLong(cursor.getColumnIndex("qtd_pomodoro"));
                Tarefa.Status status = cursor.getString(cursor.getColumnIndex("status")).equals(Tarefa.Status.INICIADO.toString()) ? Tarefa.Status.INICIADO : Tarefa.Status.CONCLUIDO;
                Tarefa t = new Tarefa(Id, nome, descricao, qtdPomodoro, status);
                tarefa = t;
            }
        }
        return tarefa;
    }

    @Override
    public List<Tarefa> findAll() {

        List<Tarefa> list = new ArrayList<Tarefa>();

        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, "_id");

        if (cursor.getCount() > 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);

                Long id = cursor.getLong(cursor.getColumnIndex("_id"));
                String nome = cursor.getString(cursor.getColumnIndex("nome"));
                String descricao = cursor.getString(cursor.getColumnIndex("descricao"));
                Long qtdPomodoro = cursor.getLong(cursor.getColumnIndex("qtd_pomodoro"));
                Tarefa.Status status = cursor.getString(cursor.getColumnIndex("status")).equals(Tarefa.Status.INICIADO.toString()) ? Tarefa.Status.INICIADO : Tarefa.Status.CONCLUIDO;
                Tarefa tarefa = new Tarefa(id, nome, descricao, qtdPomodoro, status);

                list.add(tarefa);

            }
        }

        return list;
    }

    @Override
    protected ContentValues createContentValue(Tarefa tarefa) {
        ContentValues values = new ContentValues();
        values.put("_id", tarefa.getId());
        values.put("nome", tarefa.getNome());
        values.put("descricao", tarefa.getDescricao());
        values.put("qtd_pomodoro", tarefa.getQtdPomodoros());
        values.put("status", tarefa.getStatus().toString());
        return values;
    }

}
