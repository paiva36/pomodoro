package fa7.edu.br.pomodoro.util;

/**
 * Created by bruno on 31/08/15.
 */
public interface NotifyAdapter {

    void dataChanged();

}
