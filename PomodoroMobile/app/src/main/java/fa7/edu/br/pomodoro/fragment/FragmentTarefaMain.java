package fa7.edu.br.pomodoro.fragment;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import fa7.edu.br.pomodoro.R;
import fa7.edu.br.pomodoro.activity.MainActivity;
import fa7.edu.br.pomodoro.adapter.TarefaAdapter;
import fa7.edu.br.pomodoro.dao.TarefaDao;
import fa7.edu.br.pomodoro.model.Tarefa;
import fa7.edu.br.pomodoro.util.NotifyAdapter;
import fa7.edu.br.pomodoro.util.RecyclerViewOnClickListener;


public class FragmentTarefaMain extends Fragment implements RecyclerViewOnClickListener, AdapterView.OnItemClickListener, View.OnClickListener, NotifyAdapter {

    private static final int INSERIR_NOVA_TAREFA = 1;
    private static final int ATUALIZAR_TAREFA = 2;
    private AppCompatActivity mMainActivity;
    private Toolbar mMainToolBar;
    private ListView navMenu;
    private TarefaDao mTarefaDao;
    private TarefaAdapter mTarefaAdapter;
    private RecyclerView mRecyclerView;
    private FloatingActionButton mFloatingActionButton;

    private Chronometer mCronometro;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tarefa_main, container, false);

        mMainActivity = (AppCompatActivity) getActivity();

        //Adicionando a ToolBar
        mMainToolBar = (Toolbar) mMainActivity.findViewById(R.id.main_toolbar);
        mMainToolBar.setTitle("Pomodoro App");
        mMainToolBar.setSubtitle("Gerencia de Tempo Eficiente");
        mMainToolBar.setLogo(R.mipmap.ic_launcher);
        mMainActivity.setSupportActionBar(mMainToolBar);
        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        mCronometro = (Chronometer) mMainActivity.findViewById(R.id.cronometro);

        List<String> navMenuList = new ArrayList<>();
        navMenuList.add("Item 1");
        navMenuList.add("Item 2");
        navMenuList.add("Item 3");

        // Vinculando (binding) com componente de tela...
        navMenu = (ListView) mMainActivity.findViewById(R.id.nav_drawer_menu);

        // Configurando um adaptador para o compoente
        navMenu.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, navMenuList));

        // Ativando o observer de enventos para componente.
        navMenu.setOnItemClickListener(this);

        // Instanciando o Dao de TArefas
        mTarefaDao = new TarefaDao(mMainActivity);
        List<Tarefa> tarefas = mTarefaDao.findAll();

        LinearLayoutManager llm = new LinearLayoutManager(mMainActivity);
        mTarefaAdapter = new TarefaAdapter(mMainActivity, tarefas);
        mTarefaAdapter.setListener(this);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.fragment_main_tarefa);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(mTarefaAdapter);
        mRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(mMainActivity, mRecyclerView, this));

        mFloatingActionButton = (FloatingActionButton) v.findViewById(R.id.fragment_main_floating_add_button);
        mFloatingActionButton.setOnClickListener(this);
        mFloatingActionButton.attachToRecyclerView(mRecyclerView);
        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FragmentTransaction fragmentTransaction = mMainActivity.getSupportFragmentManager().beginTransaction();
        switch (position){
            case 0:
                //FragmentSettings fragmentSettings = new FragmentSettings();
                //fragmentTransaction.replace(R.id.main_content, fragmentSettings);
                break;
            case 1:
                break;
            case 2:
                break;
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v, int position) {
        Toast.makeText(getActivity(), "Item " + position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction fragmentTransaction = mMainActivity.getSupportFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.fragment_main_floating_add_button :
                Bundle bundle = new Bundle();
                bundle.putInt("type", INSERIR_NOVA_TAREFA);
                bundle.putSerializable("adapter", mTarefaAdapter);

                FragmentTarefaForm fragmentTarefaForm = ((MainActivity)mMainActivity).getmFragmentTarefaForm();
                fragmentTarefaForm.setArguments(bundle);

                fragmentTransaction.replace(R.id.main_content, fragmentTarefaForm);
                break;
        }
        fragmentTransaction.commit();
    }


    private class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener {

        private Context mContext;
        private GestureDetector mGestureDetector;
        private RecyclerViewOnClickListener mListener;

        public RecyclerViewTouchListener(final Context context, final RecyclerView recyclerView, RecyclerViewOnClickListener listener) {

            this.mContext = context;
            this.mListener = listener;

            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    View v = mRecyclerView.findChildViewUnder(e.getX(), e.getY());
                    boolean callContextMenuStatus = false;

                    if(v instanceof CardView){
                        float x = ((RelativeLayout) ((CardView) v).getChildAt(0)).getChildAt(3).getX();
                        float w = ((RelativeLayout) ((CardView) v).getChildAt(0)).getChildAt(3).getWidth();
                        float h = ((RelativeLayout) ((CardView) v).getChildAt(0)).getChildAt(3).getHeight();
                        float y;

                        Rect rect = new Rect();
                        ((RelativeLayout) ((CardView) v).getChildAt(0)).getChildAt(3).getGlobalVisibleRect(rect);
                        y = rect.top;

                        if( e.getX() >= x && e.getX() <= w + x && e.getRawY() >= y && e.getRawY() <= h + y ){
                            callContextMenuStatus = true;
                        }

                    }

                    if(v != null && mListener != null && !callContextMenuStatus){
                        mListener.onClick(v,
                                recyclerView.getChildAdapterPosition(v));
                    }

                    return true;
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            mGestureDetector.onTouchEvent(e);
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        dataChanged();
    }

    @Override
    public void dataChanged() {
        mTarefaAdapter.getList().clear();
        mTarefaAdapter.getList().addAll(mTarefaDao.findAll());
        mTarefaAdapter.notifyDataSetChanged();
    }
}
