package fa7.edu.br.pomodoro.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fa7.edu.br.pomodoro.R;
import fa7.edu.br.pomodoro.activity.MainActivity;
import fa7.edu.br.pomodoro.dao.TarefaDao;
import fa7.edu.br.pomodoro.fragment.FragmentCronometro;
import fa7.edu.br.pomodoro.fragment.FragmentTarefaForm;
import fa7.edu.br.pomodoro.model.ContextMenuItem;
import fa7.edu.br.pomodoro.model.Tarefa;
import fa7.edu.br.pomodoro.util.RecyclerViewOnClickListener;


/**
 * Created by paiva on 09/09/2015.
 */
public class TarefaAdapter extends RecyclerView.Adapter<TarefaAdapter.TarefaViewHolder> implements Serializable {

    private static final int ATUALIZAR_TAREFA = 2;

    private Context mContext;
    private List<Tarefa> list;
    private LayoutInflater layoutInflater;
    private RecyclerViewOnClickListener listener;

    private AppCompatActivity mMainActivity;

    private TarefaDao tarefaDao;

    private Tarefa tarefa;

    public TarefaAdapter(Context context, List<Tarefa> list) {
        this.mContext = context;
        this.list = list;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.tarefaDao = new TarefaDao(context);
    }

    public void setListener(RecyclerViewOnClickListener listener) {
        this.listener = listener;
    }

    public List<Tarefa> getList() {return list;}

    @Override
    public TarefaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = null;
        v = layoutInflater.inflate(R.layout.cardview_item, viewGroup, false);
        TarefaViewHolder pvh = new TarefaViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(TarefaViewHolder tarefaViewHolder, int i) {
        Tarefa t = list.get(i);
        tarefaViewHolder.mTarefaImage.setImageResource(R.drawable.ic_tarefa);
        tarefaViewHolder.mTarefaNome.setText(t.getNome());
        tarefaViewHolder.mTarefaDescricao.setText(t.getDescricao());
        tarefaViewHolder.mTarefaQtdPomodoro.setText("Pomodoro: " + t.getQtdPomodoros());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class TarefaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private static final int EDIT_ACTION = 0;
        private static final int DELETE_ACTION = 1;
        private static final int INICIAR_TAREFA = 2;

        private ImageView mTarefaImage;
        private TextView mTarefaNome;
        private TextView mTarefaDescricao;
        private TextView mTarefaQtdPomodoro;
        private ImageView mContextMenu;

        public TarefaViewHolder(View itemView) {
            super(itemView);

//            itemView.setOnClickListener(this);

              mTarefaImage = (ImageView) itemView.findViewById(R.id.cardview_imagem_tarefa);
              mTarefaNome = (TextView) itemView.findViewById(R.id.cardview_nome_tarefa);
              mTarefaDescricao = (TextView) itemView.findViewById(R.id.cardview_descricao_tarefa);
              mTarefaQtdPomodoro = (TextView) itemView.findViewById(R.id.cardview_label_pomodoro);

              mContextMenu = (ImageView) itemView.findViewById(R.id.cardview_context_menu);
              mContextMenu.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            float scale = mContext.getResources().getDisplayMetrics().density;

            List<ContextMenuItem> items = new ArrayList<>();
            items.add(new ContextMenuItem(R.drawable.pencil, "Editar"));
            items.add(new ContextMenuItem(R.drawable.delete, "Excluir"));
            items.add(new ContextMenuItem(R.drawable.ic_tarefa, "Iniciar"));

            ContextMenuAdapter adapter = new ContextMenuAdapter(mContext, items);

            ListPopupWindow listPopupWindow = new ListPopupWindow(mContext);
            listPopupWindow.setAdapter(adapter);
            listPopupWindow.setAnchorView(mContextMenu);
            listPopupWindow.setWidth((int) (240 * scale + 0.5f));
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    FragmentTransaction mFragmentTransaction = ((MainActivity) mContext).getSupportFragmentManager().beginTransaction();
                    tarefa = list.get(getAdapterPosition());
                    switch (position) {

                        case EDIT_ACTION:
                            if (tarefa == null) {
                                Log.i("[TarefaAdapter]", "Tarefa Nao encontrada");
                                Toast.makeText(mContext, "Tarefa Nao encontrada", Toast.LENGTH_SHORT).show();
                                break;
                            } else {
                                Log.i("[TarefaAdapter]", "ID Tarefa : " + String.valueOf(tarefa.getId()));
                                Toast.makeText(mContext, "Editar tarefa " + getAdapterPosition(), Toast.LENGTH_SHORT).show();
                                Bundle bundle = new Bundle();
                                bundle.putInt("type", ATUALIZAR_TAREFA);
                                bundle.putLong("idTarefa", tarefa.getId());
                                //bundle.putSerializable("tarefa", tarefa);
                                FragmentTarefaForm mFragmentTarefaForm = ((MainActivity) mContext).getmFragmentTarefaForm();
                                mFragmentTarefaForm.setArguments(bundle);
                                mFragmentTransaction.replace(R.id.main_content, mFragmentTarefaForm);
                            }
                            break;
                        case DELETE_ACTION:
                            if (tarefa == null) {
                                Log.i("[TarefaAdapter]", "Tarefa Nao encontrada");
                                break;
                            } else {
                                Log.i("[TarefaAdapter]", "ID Tarefa : " + String.valueOf(tarefa.getId()));
                            }
                            tarefaDao.delete(tarefa);
                            list = tarefaDao.findAll();
                            for (Tarefa t : list) {
                                Log.i("[TarefaAdapter]", String.valueOf(tarefa.getId()));
                            }
                            notifyItemRemoved(getAdapterPosition());
                            break;
                        case INICIAR_TAREFA:
                            if (tarefa == null) {
                                Log.i("[TarefaAdapter]", "Tarefa Nao encontrada");
                                break;
                            } else {
                                Log.i("[TarefaAdapter]", "ID Tarefa : " + String.valueOf(tarefa.getId()));
                            }
                            Bundle bundle = new Bundle();
                            bundle.putLong("idTarefa", tarefa.getId());
                            FragmentCronometro mFragmentCronometro = ((MainActivity) mContext).getmFragmentCronometro();
                            mFragmentCronometro.setArguments(bundle);
                            mFragmentTransaction.replace(R.id.main_content, mFragmentCronometro);
                            break;
                    }
                    mFragmentTransaction.commit();
                }
            });
            listPopupWindow.setModal(true);
            listPopupWindow.getBackground().setAlpha(0);
            listPopupWindow.show();

        }
    }
}
