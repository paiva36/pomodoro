package fa7.edu.br.pomodoro.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import fa7.edu.br.pomodoro.R;
import fa7.edu.br.pomodoro.activity.MainActivity;
import fa7.edu.br.pomodoro.dao.TarefaDao;
import fa7.edu.br.pomodoro.model.Tarefa;

import static android.widget.Toast.makeText;


public class FragmentTarefaForm extends Fragment {

    private static final int INSERIR_NOVA_TAREFA = 1;
    private static final int ATUALIZAR_TAREFA = 2;

    private AppCompatActivity mMainActivity;
    private Toolbar mMainToolBar;
    private TextView mNome;
    private TextView mDescricao;
    private TextView mQtdPomodoro;
    private TarefaDao mTarefaDao;

    private int tipoOp;
    private Long idTarefaSelecionada;
    private Tarefa tarefa;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_tarefa_form, container, false);
        mMainActivity = (AppCompatActivity) getActivity();

        mMainToolBar = (Toolbar) mMainActivity.findViewById(R.id.main_toolbar);
        mMainToolBar.setLogo(null);
        mMainToolBar.setSubtitle("");
        mMainActivity.setSupportActionBar(mMainToolBar);
        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mNome = (TextView) v.findViewById(R.id.fragment_form_nome_tarefa);
        mDescricao = (TextView) v.findViewById(R.id.fragment_form_descricao_tarefa);
        mQtdPomodoro = (TextView) v.findViewById(R.id.fragment_form_qtd_pomodoro);

        mTarefaDao = new TarefaDao(mMainActivity);

        Bundle b = getArguments();
        tipoOp = b.getInt("type");
        if (tipoOp == INSERIR_NOVA_TAREFA) {
            mMainToolBar.setTitle("Nova Tarefa");
        } else {
            mMainToolBar.setTitle("Atualiza Tarefa");
            if (tipoOp == ATUALIZAR_TAREFA) {
                idTarefaSelecionada = b.getLong("idTarefa");
                if (idTarefaSelecionada != null) {
                    tarefa = mTarefaDao.find(idTarefaSelecionada);
                    mNome.setText(tarefa.getNome());
                    mDescricao.setText(tarefa.getDescricao());
                    mQtdPomodoro.setText(String.valueOf(tarefa.getQtdPomodoros()));
                }
            }
        }
        return v;
    }

    @Override
    public void onStop() {
        super.onStop();
        final AlertDialog.Builder builder = new AlertDialog.Builder(mMainActivity);
        switch (tipoOp) {
            case INSERIR_NOVA_TAREFA :
                makeText(getActivity(), "Dados salvos com sucesso", Toast.LENGTH_LONG).show();
                    builder
                        .setTitle("Salvar Dados !")
                        .setMessage("Voce deseja salvar estes dados ?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String nome = mNome.getText().toString();
                                String descricao = mDescricao.getText().toString();
                                Long qtdPomodoro = Long.parseLong(mQtdPomodoro.getText().toString());
                                Tarefa tarefa = new Tarefa(nome, descricao, qtdPomodoro);
                                mTarefaDao.insert(tarefa);
                                ((MainActivity) mMainActivity).getmFragmentTarefaMain().dataChanged();
                            }
                        }).setNegativeButton("Nao", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                break;
            case ATUALIZAR_TAREFA :
                makeText(getActivity(), "Dados Atualizados com sucesso", Toast.LENGTH_LONG).show();
                    builder
                        .setTitle("Alterar Dados !")
                        .setMessage("Voce deseja alterar estes dados ?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String nome = mNome.getText().toString();
                                String descricao = mDescricao.getText().toString();
                                Long qtdPomodoro = Long.parseLong(mQtdPomodoro.getText().toString());
                                tarefa.setNome(nome);
                                tarefa.setDescricao(descricao);
                                tarefa.setQtdPomodoros(qtdPomodoro);
                                mTarefaDao.update(tarefa);
                                ((MainActivity) mMainActivity).getmFragmentTarefaMain().dataChanged();
                            }
                        }).setNegativeButton("Nao", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                break;
        }
        builder.create().show();
    }
}