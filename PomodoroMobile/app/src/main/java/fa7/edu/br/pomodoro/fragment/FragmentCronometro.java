package fa7.edu.br.pomodoro.fragment;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.TextView;

import fa7.edu.br.pomodoro.R;
import fa7.edu.br.pomodoro.dao.TarefaDao;
import fa7.edu.br.pomodoro.model.Tarefa;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentCronometro extends Fragment {


    private AppCompatActivity mMainActivity;
    private Toolbar mMainToolBar;
    private TarefaDao mTarefaDao;

    private Chronometer chronometer;

    private TextView cronometroText;

    private  MediaPlayer mMediaPlayer;

    public FragmentCronometro() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment_cronometro, container, false);
        mMainActivity = (AppCompatActivity) getActivity();
        mMainToolBar = (Toolbar) mMainActivity.findViewById(R.id.main_toolbar);
        mMainToolBar.setLogo(null);
        mMainToolBar.setSubtitle("");
        mMainActivity.setSupportActionBar(mMainToolBar);
        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mTarefaDao = new TarefaDao(mMainActivity);

        Bundle b = getArguments();
        mMainToolBar.setTitle("Contador regressivo da tarefa");
        Long idTarefaSelecionada = b.getLong("idTarefa");
        Tarefa tarefa = mTarefaDao.find(idTarefaSelecionada);

        cronometroText = (TextView) v.findViewById(R.id.cronometro);

        Long ciclos = tarefa.getQtdPomodoros();

        Long tempoDuracao = ciclos*(25*60*1000);
        mMediaPlayer = MediaPlayer.create(mMainActivity, R.raw.create_database);

        CountDownTimer countDownTimer = new CountDownTimer(tempoDuracao, 1000) {
            public void onTick(long millisUntilFinished) {
                Long segundos = ( millisUntilFinished / 1000 ) % 60;
                Long minutos  = ( millisUntilFinished / 60000 ) % 60;     // 60000   = 60 * 1000
                Long horas    = millisUntilFinished / 3600000;            // 3600000 = 60 * 60 * 1000
                String tempoRestante = String.format("%02d:%02d:%02d", horas, minutos, segundos);
                cronometroText.setText(tempoRestante);
            }
            public void onFinish() {
                mMediaPlayer.start();
                cronometroText.setText("Fim!");
            }
        };
        countDownTimer.start();
        return v;
    }


}
